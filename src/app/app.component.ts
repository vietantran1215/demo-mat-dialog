import { Component } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from './components/dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  dialogRef?: MatDialogRef<DialogComponent>;
  title = 'demo-material';
  constructor(private readonly dialog: MatDialog) { }
  showDialog() {
    this.dialogRef = this.dialog.open(DialogComponent, { data: { email: 'tranvietan@gmail.com' } });
    this.dialogRef.afterClosed().subscribe(res => {
      const result = res ? "Confirm": "Discard";
      console.log(result);
    })
  }
}
